package com.hendisantika.springbootapiservices.repository;

import com.hendisantika.springbootapiservices.entity.Kota;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-api-services
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 25/12/19
 * Time: 07.21
 */
@Repository
public interface KotaRepository extends JpaRepository<Kota, Long> {
    List<Kota> findAllByOrderByIdAsc();
}