package com.hendisantika.springbootapiservices.service;

import com.hendisantika.springbootapiservices.entity.Kota;
import com.hendisantika.springbootapiservices.repository.KotaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-api-services
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 25/12/19
 * Time: 07.24
 */
@Service
@Transactional
public class KotaServiceImpl implements KotaService {

    @Autowired
    private KotaRepository kotaRepository;

    @Override
    public List<Kota> findAll(Pageable pageable) {
        return kotaRepository.findAllByOrderByIdAsc();
    }

    @Override
    public Kota findById(Long id) {

        return kotaRepository.getOne(id);
    }

    @Override
    public Kota create(Kota kota) {
        return kotaRepository.save(kota);
    }

    @Override
    public Kota update(Long id, Kota kota) {
        kota.setId(id);
        return kotaRepository.save(kota);
    }

    @Override
    public Kota delete(Long id) {
        Kota kota = findById(id);
        kotaRepository.deleteById(id);
        return kota;
    }

}