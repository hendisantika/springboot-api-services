package com.hendisantika.springbootapiservices.service;

import com.hendisantika.springbootapiservices.entity.Negara;
import com.hendisantika.springbootapiservices.repository.NegaraRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-api-services
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 25/12/19
 * Time: 07.25
 */
@Service
@Transactional
public class NegaraServiceImpl implements NegaraService {

    @Autowired
    private NegaraRepository negaraRepository;

    @Override
    public List<Negara> findAll(Pageable pageable) {
        return negaraRepository.findAllByOrderByIdAsc();
    }

    @Override
    public Negara findById(Long id) {

        return negaraRepository.getOne(id);
    }

    @Override
    public Negara create(Negara negara) {
        return negaraRepository.save(negara);
    }

    @Override
    public Negara update(Long id, Negara negara) {
        negara.setId(id);
        return negaraRepository.save(negara);
    }

    @Override
    public Negara delete(Long id) {
        Negara negara = findById(id);
        negaraRepository.deleteById(id);
        return negara;
    }

}