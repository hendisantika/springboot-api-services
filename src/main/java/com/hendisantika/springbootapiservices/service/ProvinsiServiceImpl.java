package com.hendisantika.springbootapiservices.service;

import com.hendisantika.springbootapiservices.entity.Provinsi;
import com.hendisantika.springbootapiservices.repository.ProvinsiRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-api-services
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 25/12/19
 * Time: 07.25
 */
@Service
@Transactional
public class ProvinsiServiceImpl implements ProvinsiService {

    @Autowired
    private ProvinsiRepository provinsiRepository;

    @Override
    public List<Provinsi> findAll(Pageable pageable) {
        return provinsiRepository.findAllByOrderByIdAsc();
    }

    @Override
    public Provinsi findById(Long id) {

        return provinsiRepository.getOne(id);
    }

    @Override
    public Provinsi create(Provinsi provinsi) {
        return provinsiRepository.save(provinsi);
    }

    @Override
    public Provinsi update(Long id, Provinsi provinsi) {
        provinsi.setId(id);
        return provinsiRepository.save(provinsi);
    }

    @Override
    public Provinsi delete(Long id) {
        Provinsi provinsi = findById(id);
        provinsiRepository.deleteById(id);
        return provinsi;
    }

}