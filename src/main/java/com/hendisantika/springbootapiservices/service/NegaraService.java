package com.hendisantika.springbootapiservices.service;

import com.hendisantika.springbootapiservices.entity.Negara;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-api-services
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 25/12/19
 * Time: 07.23
 */
public interface NegaraService {

    /**
     * Find all City
     *
     * @param pageable paging parameter
     * @return list of content
     */
    List<Negara> findAll(Pageable pageable);

    /**
     * Find City by Id
     *
     * @param id id of content
     * @return selected content
     */
    Negara findById(Long id);

    /**
     * Create City
     *
     * @param negara
     * @return created content
     */
    Negara create(Negara negara);

    /**
     * Update City based on Id
     *
     * @param id     id of content
     * @param negara
     * @return updated content
     */
    Negara update(Long id, Negara negara);

    /**
     * Delete City based on Id
     *
     * @param id id of content
     * @return deleted content
     */
    Negara delete(Long id);
}

