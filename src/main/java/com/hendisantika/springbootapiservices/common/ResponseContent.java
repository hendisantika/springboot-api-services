package com.hendisantika.springbootapiservices.common;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-api-services
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 25/12/19
 * Time: 07.19
 */
@Setter
@Getter
@Builder
public class ResponseContent {
    private String result;
    private String message;
    private Long timestamp;
    private Object data;
}