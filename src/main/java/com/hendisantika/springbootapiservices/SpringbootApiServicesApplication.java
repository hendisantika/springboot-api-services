package com.hendisantika.springbootapiservices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootApiServicesApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootApiServicesApplication.class, args);
	}

}
