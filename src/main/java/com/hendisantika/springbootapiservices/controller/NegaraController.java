package com.hendisantika.springbootapiservices.controller;

import com.hendisantika.springbootapiservices.common.ResponseContent;
import com.hendisantika.springbootapiservices.common.ResponseResult;
import com.hendisantika.springbootapiservices.entity.Negara;
import com.hendisantika.springbootapiservices.service.NegaraService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-api-services
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 25/12/19
 * Time: 07.29
 */
@Slf4j
@RestController
@RequestMapping("negara")
public class NegaraController {

    @Autowired
    private NegaraService negaraService;

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity findAll(Pageable pageable) {
        return ResponseEntity.ok(ResponseContent.builder()
                .result(ResponseResult.SUCCESS)
                .timestamp(System.currentTimeMillis())
                .message("Success Find All Negara")
                .data(negaraService.findAll(pageable))
                .build()
        );
    }

    @GetMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity findById(@PathVariable("id") Long id) {
        return ResponseEntity.ok(ResponseContent.builder()
                .result(ResponseResult.SUCCESS)
                .timestamp(System.currentTimeMillis())
                .message("Success Find By Id Negara")
                .data(negaraService.findById(id))
                .build()
        );
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity create(@RequestBody @Validated Negara negara) {
        return ResponseEntity.ok(ResponseContent.builder()
                .result(ResponseResult.SUCCESS)
                .timestamp(System.currentTimeMillis())
                .message("Success Create Negara")
                .data(negaraService.create(negara))
                .build()
        );
    }

    @PutMapping(value = "{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity update(@PathVariable("id") Long id, @RequestBody @Validated Negara negara) {
        return ResponseEntity.ok(ResponseContent.builder()
                .result(ResponseResult.SUCCESS)
                .timestamp(System.currentTimeMillis())
                .message("Success Update Negara")
                .data(negaraService.update(id, negara))
                .build()
        );
    }

    @DeleteMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity delete(@PathVariable("id") Long id) {
        return ResponseEntity.ok(ResponseContent.builder()
                .result(ResponseResult.SUCCESS)
                .timestamp(System.currentTimeMillis())
                .message("Success Delete Negara")
                .data(negaraService.delete(id))
                .build()
        );
    }
}
